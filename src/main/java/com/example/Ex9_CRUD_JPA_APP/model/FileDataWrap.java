package com.example.Ex9_CRUD_JPA_APP.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FileDataWrap {
    private List<FileData> all;
}
