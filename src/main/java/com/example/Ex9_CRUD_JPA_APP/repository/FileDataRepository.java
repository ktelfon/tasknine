package com.example.Ex9_CRUD_JPA_APP.repository;

import com.example.Ex9_CRUD_JPA_APP.model.FileData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDataRepository extends JpaRepository<FileData, String> {
}
