package com.example.Ex9_CRUD_JPA_APP.exception;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}