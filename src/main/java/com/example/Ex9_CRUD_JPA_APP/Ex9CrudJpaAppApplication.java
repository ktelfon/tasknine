package com.example.Ex9_CRUD_JPA_APP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ex9CrudJpaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ex9CrudJpaAppApplication.class, args);
	}

}
